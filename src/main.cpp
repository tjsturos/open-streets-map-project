#include <optional>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <io2d.h>
#include "route_model.h"
#include "render.h"
#include "route_planner.h"

using namespace std::experimental;

static std::optional<std::vector<std::byte>> ReadFile(const std::string &path)
{   
    std::ifstream is{path, std::ios::binary | std::ios::ate};
    if( !is )
        return std::nullopt;
    
    auto size = is.tellg();
    std::vector<std::byte> contents(size);    
    
    is.seekg(0);
    is.read((char*)contents.data(), size);

    if( contents.empty() )
        return std::nullopt;
    return std::move(contents);
}

// Based off of https://www.tutorialspoint.com/how-to-check-if-input-is-numeric-in-cplusplus
// and modified to accept floats.
bool IsValidInput(std::string input) {
    int period_count = 0;
    for (int i = 0; i < input.length(); i++) {
        if (input[i] == '.' && period_count == 1 || input[i] != '.' && isdigit(input[i]) == false) {
            return false;
        } else if (input[i] == '.') {
            period_count++;
        }
    }

    float numeric_input = std::stof(input);
    if (numeric_input >= 0 && numeric_input <= 100) {
        return true;
    }
    return false;
}

// https://stackoverflow.com/questions/4745858/stdcin-getline-vs-stdcin
// used to decide whether I should accept input as a float (had trouble)
// with letters as inputs, or as strings.  With strings, it doesn't fail
// the input cycle and can then pass it to validation.
float GetInput(std::string prompt)
{
    std::string input;
    bool valid_input = false;
    do {
        std::cout << prompt << ": ";
        std::getline (std::cin, input);
        if (IsValidInput(input)) {
            valid_input = true;
        } else {
            std::cout << "Enter a whole number between 0 and 100, inclusive!\n";
        }
    
    } while (!valid_input);

    return std::stof(input);;
}

int main(int argc, const char **argv)
{    
    std::string osm_data_file = "";
    if( argc > 1 ) {
        for( int i = 1; i < argc; ++i )
            if( std::string_view{argv[i]} == "-f" && ++i < argc )
                osm_data_file = argv[i];
    }
    else {
        std::cout << "To specify a map file use the following format: " << std::endl;
        std::cout << "Usage: [executable] [-f filename.osm]" << std::endl;
        osm_data_file = "../map.osm";
    }
    
    std::vector<std::byte> osm_data;
 
    if( osm_data.empty() && !osm_data_file.empty() ) {
        std::cout << "Reading OpenStreetMap data from the following file: " <<  osm_data_file << std::endl;
        auto data = ReadFile(osm_data_file);
        if( !data )
            std::cout << "Failed to read." << std::endl;
        else
            osm_data = std::move(*data);
    }
    
    float start_x = GetInput("Enter start x");
    float start_y = GetInput("Enter start y");
    float end_x = GetInput("Enter end x");
    float end_y = GetInput("Enter end y");
    
    // Build Model.
    RouteModel model{osm_data};

    // Create RoutePlanner object and perform A* search.
    RoutePlanner route_planner{model, start_x, start_y, end_x, end_y};
    route_planner.AStarSearch();

    std::cout << "Distance: " << route_planner.GetDistance() << " meters. \n";

    // Render results of search.
    Render render{model};

    auto display = io2d::output_surface{400, 400, io2d::format::argb32, io2d::scaling::none, io2d::refresh_style::fixed, 30};
    display.size_change_callback([](io2d::output_surface& surface){
        surface.dimensions(surface.display_dimensions());
    });
    display.draw_callback([&](io2d::output_surface& surface){
        render.Display(surface);
    });
    display.begin_show();
}
