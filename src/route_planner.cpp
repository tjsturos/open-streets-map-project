#include "route_planner.h"
#include <algorithm>

RoutePlanner::RoutePlanner(RouteModel &model, float start_x, float start_y, float end_x, float end_y): m_Model(model) 
{
    // Convert inputs to percentage:
    start_x *= 0.01;
    start_y *= 0.01;
    end_x *= 0.01;
    end_y *= 0.01;

    // Find the closest nodes to the starting and ending coordinates.
    // Store the nodes you find in the RoutePlanner's start_node and end_node attributes.
    start_node = &m_Model.FindClosestNode(start_x, start_y);
    end_node = &m_Model.FindClosestNode(end_x, end_y);
}

// get the distance to the end from the given node
float RoutePlanner::CalculateHValue(RouteModel::Node const *node) 
{
    return node->distance(*end_node);
}

// Expand the current node by adding all unvisited neighbors to the open list.
void RoutePlanner::AddNeighbors(RouteModel::Node *current_node)
{
    current_node->FindNeighbors();

    for(RouteModel::Node *neighbor : current_node->neighbors) {
        neighbor->parent = current_node;
        neighbor->g_value = current_node->g_value + neighbor->distance(*current_node);
        neighbor->h_value = CalculateHValue(neighbor);
        open_list.push_back(neighbor);
        neighbor->visited = true;
    }
}

// Sort the open list and return the next node.
RouteModel::Node *RoutePlanner::NextNode() 
{
    std::sort(open_list.begin(), open_list.end(), [this](RouteModel::Node *a, RouteModel::Node *b)
    {
        // f(n) = g(n) + h(n) --> https://en.wikipedia.org/wiki/A*_search_algorithm#Description
        float f1 = a->g_value + a->h_value;
        float f2 = b->g_value + b->h_value;
        return f1 > f2;
    });

    // get the last (back)  node with the smallest h+g_value sum and pop it to remove it
    RouteModel::Node *nextNode = open_list.back();
    open_list.pop_back();
    return nextNode;
}

// Return the final path found from your A* search.
std::vector<RouteModel::Node> RoutePlanner::ConstructFinalPath(RouteModel::Node *current_node) {
    // Create path_found vector
    distance = current_node->g_value;
    std::vector<RouteModel::Node> path_found;
    while (current_node != start_node) {
        path_found.insert(path_found.begin(), *current_node);
        current_node = current_node->parent;
    }

    path_found.insert(path_found.begin(), *current_node);

    distance *= m_Model.MetricScale(); // Multiply the distance by the scale of the map to get meters.
    return path_found;
}

// A* Search algorithm started/triggered here.
// https://en.wikipedia.org/wiki/A*_search_algorithm
void RoutePlanner::AStarSearch() {
    RouteModel::Node *current_node = nullptr;

    current_node = start_node;
    current_node->visited = true;
    open_list.push_back(current_node);
    while (open_list.size() > 0) {
        current_node = NextNode();
        
        if (current_node == end_node) {
            m_Model.path = ConstructFinalPath(current_node);
            break;
        }
        AddNeighbors(current_node);
    }
}
